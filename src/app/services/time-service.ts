import { Observable, timer } from 'rxjs';

export class TimeService {

  currentTime(): Observable<any> {
    return new Observable(subscriber => {
      // Hint: use timer to trigger an event every second
      // timer(1000, 1000) -> start emitting after 1000 millis and subsequently every 1000 millis
    });
  }
}
